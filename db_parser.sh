#!/bin/bash
# (c) GNU GPLv3 - Mablr
cat db_dump | grep "INSERT INTO \`virtual_users" | sed "s/(/\n/g;s/)\(,\|;\|\)//g;s/'/\"/g;s/{SHA256-CRYPT}/{CRYPT}/g" |  cut -d, -f1,2,6 --complement > .dump
for user in $(cat a_supprimer); do sed -i "s/$user/$user\_\_\_\_\_\_/g;" .dump; done
sed -i "s/\ /_/g" .dump
grep -v "______" .dump | grep -v "INSERT INTO" > .dump0

for line in $(cat .dump0)
do
    mail=$(echo $line | awk -F, '{print $1}' | sed "s/\"//g")
    passwd=$(echo $line | awk -F, '{print $2}' | sed "s/\"//g;s/\//#/g")
    name=$(echo $line | awk -F, '{print $3}' | sed "s/\"//g")
    uid=$(echo $mail | awk -F"@" '{print $1}')
    sed '1!d' model.ldif | sed "s/%uid%/$uid/g" >> libmail.ldif
    sed '2!d' model.ldif | sed "s/%name%/$name/g" >> libmail.ldif
    sed '3!d' model.ldif >> libmail.ldif
    sed '4!d' model.ldif | sed "s/%uid%/$uid/g" >> libmail.ldif
    sed '5!d' model.ldif | sed "s/%passwd%/$passwd/g;s/#/\//g" >> libmail.ldif
    sed '6,9 !d' model.ldif >> libmail.ldif
    sed '10!d' model.ldif | sed "s/%mail%/$mail/g" >> libmail.ldif
    sed '11,12 !d' model.ldif >> libmail.ldif
    echo >> libmail.ldif
done
rm .dump .dump0
